# EJERCICIOS MÓDULO BACK-END

* [Ejercicio 01 NODEJS.](back-ej01)
* [Ejercicio 02 NODEJS.](back-ej02)
* [Ejercicio 03 NODEJS.](back-ej03)

## 1. NODE.JS

Implementar una aplicación de consola para calcular la cuota mensual de un préstamo. Para ello, la aplicación debe:

* Usar una librería para realizar los cálculos necesarios.
* Recibir por línea de comandos los argumentos necesarios para realizar los cálculos.
* El resultado debe aparecer en la consola.
* Usar directamente la API de NodeJS (process.argv).

Ejemplo para insertar en consola:

```sh
(cantidad, duración del prestamo -en meses- e intereses)
$ node index.js 10000 12 0.5
```

* [Ejercicio 01 NODEJS.](back-ej01)

## 2. NODE.JS

Un cliente nos pide realizar un sistema para gestionar eventos culturales. Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.

Opcionalmente pueden incluir una descripción. El cliente necesitará una API REST para añadir conciertos y poder obteneruna lista de los existentes.
El objetivo del ejercicio es que traduzcas estos requisitos a una descripcióntécnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles son los código de error a devolver

 Notas:

* El conocimiento necesario para realizarlo es el adquirido hasta la clase del miércoles.
* Llega con un endpoint GET y otro POST
* El almacenamiento será en memoria, por tanto cuando se cierre el servidor se perderán los datos. De momento es aceptable esto.

```sh
(valores: evento, nombre, aforo y artista)
localhost:3000/events
```

* [Ejercicio 02 NODEJS.](back-ej02)

## 3. NODE.JS

 Una tienda quiere ofrecer sus servicios online. Para ello se dispone a digitalizar su catálogo y mostrarlo en una web. Nos piden realizar la parte de backend, que debe permitir:

```sh
Acceso con:
email: adolfo@maragoto.com
password: 1234

token: '$2b$10$THvD793Fq3ZerL3bs4N5Lu2u04IWUAw1COWeIgzi.AsnFCn7vKI/G'

localhost:8080/login
```

* Añadir productos

```sh
Añadir productos: nombre / stock / precio
localhost:8080/producto
```

* modificar productos, (es necesario que el usuario esté autenticado)

```sh
Modificar productos PUT:
nombre / stock / precio
localhost:8080/producto/?id=1
```

* Listar Productos (no es necesario estar autenticado)

```sh
Listar productos:
localhost:8080/producto
```

Nota:

* No se pueden dar de alta usuarios (Deberá existir uno por defecto para las tareas de administración)
* Deberá poder filtrarla mediante parámetros enviados en la `querystring`

```sh
Filtrar en querystring (por nombre):
localhost:8080/producto/filtrado?nombre=nombre-producto
```

* [Ejercicio 03 NODEJS.](back-ej03)
