var LoanJS = require('loanjs');

// node index.js cantidad cuotas interes
// ejemplo: node index.js 10000 12 0.5
const cantidad = process.argv[2];
const cuotas = process.argv[3];
const tasaInteres = process.argv[4];

var loan = new LoanJS.Loan(
  cantidad, // cantidad
  cuotas,   // número de cuotas
  tasaInteres,    // interés
  true,  // si true, disminuyen las entregas 
  
);
console.log(loan)