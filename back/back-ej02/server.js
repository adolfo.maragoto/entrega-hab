/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 * 
 * El cliente necesitará una API REST para añadir conciertos y poder obtener
 * una lista de los existentes.
 * 
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles 
 * son los código de error a devolver
 * 
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 * 
 */

const bodyParser = require("body-parser");
const express = require("express");
const morgan = require("morgan");

const app = express();

//SETTINGS
app.set('json spaces', 2); // espaciado del json para una mejor visualización

//MIDDLEWARES
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use( bodyParser.urlencoded({extended: true}));

let events = []; 

app.get('/events', (req, res) => { 
  res.json(events);
});

app.post('/events', (req, res) => {

  // ENVIO DE DATOS
    const valueEvento = req.body.evento;
    const valueNombre = req.body.nombre;
    const valueAforo = req.body.aforo;
    const valueArtista = req.body.artista;

  let data = {
    id: events.length + 1,
    evento: valueEvento,
    nombre: valueNombre,
    aforo: valueAforo,
    artista: valueArtista,
  };

  // CONTROL DE ERRORES
  // Status 400
  // Falta por enviar algun campo o está vacío
  if (valueEvento === undefined || valueEvento === "") {
    res.status(400).send('Ups!!! te falta por escribir el evento');
    return;
  }
  if (valueNombre === undefined || valueNombre === "") {
    res.status(400).send('Ups!!! te falta escribir el nombre');
    return;
  }
  if (valueAforo === undefined || valueAforo === "") {
    res.status(400).send('Ups!!! te falta por escribir el aforo');
    return;
  }
  if (valueArtista === undefined || valueArtista === "") {
    res.status(400).send('Ups!!! te falta escribir el artista');
    return;
  }

  // El aforo no es un número 
  if (isNaN(valueAforo)) {
    res.status(400).send('Así no vale! :) El aforo debe que ser un número');
    return;
  }

  // El evento es diferente de los especificados en el ejercicio ('concierto', 'teatro' o 'monólogo')
  if (valueEvento.toUpperCase() !== "CONCIERTO" && valueEvento.toUpperCase() !== "TEATRO" && valueEvento.toUpperCase() !== "MONÓLOGO" ) {
    res.status(400).send( `De momento la categoría "${valueEvento}" no es válida. Pruebe con: concierto, teatros o monólogo.` );
    return;
  }

  // Status 409
  // El nombre está repetido en el mismo tipo de Evento
  const isEqual = (events) => {
    return ( events.evento === valueEvento && events.nombre === valueNombre ); // le devolvemos true o false
  };
  const equalElements = events.filter(isEqual);
  if (equalElements.length !== 0) { // comprobar si el elemento existe
    res.status(409).send( `Vaya!!!, parace que ya tienes registrado este nombre en  ${valueEvento}.` );
    return;
  }

  // Status 200 OK
  events.push(data);
  res.status(200).send( 'La información ha sido guardada perfectamente.' );
});

// SERVER
app.listen(3000);
