/**
 * This file is just a double to use instead of real database.
 * 
 */
let users = [
    {
        email: 'adolfo@maragoto.com',
        //passwod:1234
        password: '$2b$10$THvD793Fq3ZerL3bs4N5Lu2u04IWUAw1COWeIgzi.AsnFCn7vKI/G'
    }
];

let id = 1;

const getUser = (email) => {
    const matchEmail = user => user.email === email;

    return users.find( matchEmail );
}

const saveUser = (email, password, role) => {
    users.push({
        id: id++,
        email,
        password,
        role: role
    })
}



module.exports = {
    getUser,
    saveUser
}