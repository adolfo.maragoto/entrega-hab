// CONTENEDOR DE PRODUCTOS
let products = [];


//-----------------------------------------------------------------------------------------------------
// LISTADO DE PRODUCTOS
const list = (req, res) => {
   
    res.json(products);

}


//-----------------------------------------------------------------------------------------------------
// AÑADIR PRODUCTO
const add = (req, res) => {

    const {nombre, stock, precio} = req.body;
    
    // validar los valores recibidos
    if (!nombre && !stock && !precio) { // todos los campos vacios
        res.status(400).send('No seas tan rapido vaquero!!!, antes tienes que cubrir los campos :)');
        return;
    }
    if (!nombre) { // nombre vacio
        res.status(400).send('Uy! El campo de nombre está vacio');
        return;
    }
    if (!stock) { // stock vacio
        res.status(400).send('Eco, ecoooo! no hay nada en stock');
        return;
    }
    if (stock < 0) { // stock inferior a 0
        res.status(400).send('Uy!!! El numero de stock tiene que ser superior a 0');
        return;
    }

    if (isNaN(stock)) { // no es un numero
        res.status(400).send('Se ha colado un caracter no valido en el stock');
        return;
    }

    if (!precio) { // campo vacio
        res.status(400).send('Uy! revisa el campo de stock');
        return;
    }

    if (isNaN(precio)) { // precio no es un numero
        res.status(400).send('Nooo, no!!! en el precio no valen caracteres');
        return;
    }

    if (precio == 0) { // precio 0
        res.status(400).send('Alaaa!!! seguro que quieres regalar este producto?');
        return;
    }
    if (precio < 0) { // precio negativo
        res.status(400).send('Ojito! que el precio tiene que ser superior a 0');
        return;
    }

    const isEqual = (products) => {
        return (products.nombre === nombre); // le devolvemos true o false
    };
    const equalProducts = products.filter(isEqual);
    if (equalProducts.length !== 0) { // comprobar si el producto existe
        res.status(409).send(`Vaya!!!, parace que ya tienes el producto "${nombre}" registrado.`);
        return;
    }

    products.push({
        id: products.length + 1,
        nombre,
        stock,
        precio: (`${precio} €`)
    })
    
    // Se añaden palabras aleatorias en el res.status
    // cada vez que se añade correctamente un producto
    const randomWords = ['Bieeeen!!!', 'Perfecto!!!', 'Yupiii!!!', 'Muy bien!'];
    for (var n = 0; n < randomWords.length; n++) {
        randomWordsSelect = (randomWords[Math.floor(Math.random() * randomWords.length)]);
    }

    res.status(200).send(`${randomWordsSelect}, el producto se ha subido correctamente`);
}




//-----------------------------------------------------------------------------------------------------
// MODIFICAR PRODUCTOS POR ID
const modifyProduct = (req, res) => {

    const {nombre, stock, precio} = req.body;

    // recibimos el ID de producto
    let idProduct = req.query.id;

    let id = parseInt(idProduct) 
    if ( products.length < 1 ) { // todos los campos vacios
        res.status(400).send('Antes de quitar hay que poner. Actualmente no hay productos en la base de datos ');
        return;
    }
    if (!nombre && !stock && !precio) { // todos los campos vacios
        res.status(400).send('No seas tan rapido vaquero!!!, antes tienes que cubrir los campos :)');
        return;
    }
    if (!nombre) { // nombre vacio
        res.status(400).send('Uy! El campo de nombre está vacio');
        return;
    }
    if (!stock) { // stock vacio
        res.status(400).send('Eco, ecoooo! no hay nada en stock');
        return;
    }
    if (stock < 0) { // stock inferior a 0
        res.status(400).send('Uy!!! El numero de stock tiene que ser superior a 0');
        return;
    }

    if (isNaN(stock)) { // no es un numero
        res.status(400).send('Se ha colado un caracter no valido en el stock');
        return;
    }

    if (!precio) { // campo vacio
        res.status(400).send('Uy! revisa el campo de stock');
        return;
    }

    if (isNaN(precio)) { // precio no es un numero
        res.status(400).send('Nooo, no!!! en el precio no valen caracteres');
        return;
    }

    if (precio == 0) { // precio 0
        res.status(400).send('Alaaa!!! seguro que quieres regalar este producto?');
        return;
    }
    if (precio < 0) { // precio negativo
        res.status(400).send('Ojito! que el precio tiene que ser superior a 0');
        return;
    }
    // se busca, se modifica por los archivos req.body
    products.map(function (product) {
        if (product.id == id) {
            product.nombre = nombre;
            product.stock = stock;
            product.precio = precio;
        }

        return product;
    });

    res.status(200).send(`El producto ${nombre} se ha modificado correctamente`);

}


//-----------------------------------------------------------------------------------------------------
// FILTRADO POR QUERYSTRING
const productFiltering = (req, res) => {
    
        const filteredProductByName = req.query['nombre'].toLowerCase();
        
        filteredProducts = products.filter(product => product.nombre === filteredProductByName)
    
        // No existe el producto buscado
        if(filteredProducts <= 0 ){
            res.status(204).send('Ups!!! no existe este producto');
        }
        
        res.json(filteredProducts);
        
    }



module.exports = {
    add,
    list,
    modifyProduct,
    productFiltering
}
