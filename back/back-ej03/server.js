/**
 * Una tienda quiere ofrecer sus servicios online. 
 * Para ello se dispone a digitalizar su catálogo y mostrarlo 
 * en una web. Nos piden realizar la parte de backend, que debe
 * permitir:
 *  - añadir
 *  - modificar productos, para lo cual será 
 * necesario que el usuario esté autenticado; y permitirá también 
 * - Listar los productos existentes, que se podrá acceder 
 * libremente.
 * 
 * Notas:
 *   - no se pueden dar de alta usuarios. Deberá existir uno por
 * defecto para las tareas de administración.
 *   - la lista de productos puede llegar a ser muy grande, así 
 * que el usuario deberá poder filtrarla mediante parámetros
 * enviados en la `querystring`
 *   - la estructura de un producto es la siguiente:
 *       {
 *           name: '',
 *           stock: <número de productos disponibles de este modelo>
 *           precio: 100
 *       }
 * 
 * 
 * 
 */

require('dotenv').config();

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');

const { login } = require('./controllers/users');
const { isAuthenticated } = require('./middlewares/auth')
const { add, list, modifyProduct, productFiltering } = require('./controllers/productos');

const port = process.env.PORT;
const app = express();


// MIDDLEWARE
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());


const miMiddleware = (req, res, next) => {
    console.log('código que se ejecuta antes de procesar el endpoint');
    next();
}

const miMiddlewareWrapper = (req, res, next) => {
    return miMiddleware;
}

// hacemos login
app.post('/login', login);

// pueden crear productos (solo el usuario autenticado en este caso está insertado en el código archivo: bd_mock.js)
app.post('/producto', isAuthenticated, add);

// cualquiera puede ver el listado de productos
app.get('/producto', list, productFiltering);

// Filtrado productos por nombre 
app.get('/producto/filtrado', productFiltering);

// se puede modificar productos (solo el usuario autenticado)
app.put('/producto', isAuthenticated, modifyProduct);


app.use((error, req, res, next) => {

    res.status(error.status || 500).send({status: 'error', message: error.message})
});


// SERVIDOR
app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});