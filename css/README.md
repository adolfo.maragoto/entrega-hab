# EJERCICIO MÓDULO CSS

## 1. Entrega ejercicio 1

El objetivo de este ejercicio es reproducir lo más fielmente posible mediante CSS una imagen dada.

* [Ejercicio 01 CSS.](css-ej01)

 > Nota: Solo se podrán utilizar los css dasdos hasta la fecha.

## 2. Entrega ejercicio 2 - FLEX

Maquetar la landing pasada por el profesor utilizando Flex, anclas y translates.

* [Ejercicio 02 CSS.](css-ej02)

## 3. Entrega ejercicio 3 - GRID

Maquetar la landing pasada por el profesor utilizando Grid, Flex, importación de fuentes, Media query y animaciones.

* [Ejercicio 03 CSS.](css-ej03)
