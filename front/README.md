# EJERCICIOS MÓDULO FRONT

## 1. Entrega ejercicio 1

Construir una plataforma de música utilizando Vuejs y la API Lastfm

* [Ejercicio 01 Front.](front-ej01)

## 1. Entrega ejercicio 2

Simulacion de un mercado donde ver Productos; editar, borrar, ver y crear Clientes

* [Ejercicio 02 Front.](front-ej02)