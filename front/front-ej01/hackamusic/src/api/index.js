import config from './config.js' 
const axios = require('axios').default

const apiKey = config.apiKey
const URL_BASE = 'http://ws.audioscrobbler.com/'
const URL_GEO = `2.0/?method=geo.gettopartists&country=spain&api_key=${apiKey}&format=json`;
const URL_TAGS = `2.0/?method=album.gettoptags&artist=radiohead&album=the%20bends&api_key=${apiKey}&format=json`;
const URL_TRACKS =`2.0/?method=artist.gettoptracks&artist=cher&api_key=${apiKey}&format=json`;

// función para consegir las canciones
async function getArtists(){
    try{
     const response = await axios.get(`${URL_BASE}${URL_GEO}`)
     //console.log(response)    
    return(response)

     }catch (error){
         console.error(error)
     }
}
// función para consegir los tags
async function getTopTags(){
    try{
     const response = await axios.get(`${URL_BASE}${URL_TAGS}`)
     console.log(response)
     return(response)
     }catch (error){
         console.error(error)
     }
}
// función para consegir los tags
async function getTopTracks(){
    try{
     const response = await axios.get(`${URL_BASE}${URL_TRACKS}`)
     console.log(response)
     return(response)
     }catch (error){
         console.error(error)
     }
}

export default{
    getArtists,
    getTopTags,
    getTopTracks
}
