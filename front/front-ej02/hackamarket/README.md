# Hackamarket
Simular un mercado donde ver Productos; editar, borrar , ver y crear Clientes.

Crar una Base de Datos llamada "notas" con los siguietes tablas

<code>
CREATE TABLE IF NOT EXISTS  listaproductos (
 id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
 nombre LONGTEXT,
 stock INT,
 disponibilidad LONGTEXT,
 imagen LONGTEXT
);
</code>

<code>
CREATE TABLE IF NOT EXISTS  listaclientes (
 id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
 nombre LONGTEXT,
 usuario LONGTEXT,
 password LONGTEXT,
 email LONGTEXT,
 foto LONGTEXT
);
</code>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
