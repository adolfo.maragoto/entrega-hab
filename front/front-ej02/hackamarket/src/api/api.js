
// Declarando cosas que instalé

const cors = require('cors')
const bodyParser = require('body-parser')
const mysql = require('mysql')
const express = require('express')
const app = express()

// Cosas que usa APP

app.use(cors())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

// conexión a la BBDD
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'notas'
})

// realizando conexión a BBDD
connection.connect(error => {
    if(error) throw error
    console.log('DATABASE CONECTADA :)')
})

// PUERTO DE CONEXIÓN DEL SERVICIO
const PORT = 3050

// CONEXIÓN DEL SERVICIO
app.listen(PORT, () => console.log('API CONECTADA :)'))



// RECOGER TODOS LOS CLIENTES DE LA BASE
app.get('/', (req, res) => {
    res.send('Bienvenido a HackMarket')
})




//PRODUCTOS:----------------------------------------------------------
// RECOGIENDO TODOS LOS PRODUCTOS DE LA BASE DE DATOS
app.get('/productos', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM listaproductos'

    // conexion a la BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay productos que mostrar. :(')
        }
    })
})




//CLIENTES:----------------------------------------------------------
// OBTENER TODOS LOS CLIENTES DE LA BBDD
app.get('/clientes', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM listaclientes'

    // conexion a la BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay clientes que mostrar. :(')
        }
    })
})
// FIN OBTENER DATOS CLIENTES

// BORRANDO CLIENTES DE LA BBDD
app.delete('/delete/:id', (req, res) => {
    
    //DATOS QUE LEGAN DE LA VISTA
    const id = req.params.id

    // SECUENCIA SQL
    const sql = `DELETE FROM listaclientes WHERE id=${id}`

    // CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('cliente borrado')
    })
})
// FIN BORRAR CLIENTE
// EDITAR CLIENTE
app.put('/update/:id',(req,res) =>  {

    // DATOS QUE RECIBIMOS
    const id = req.params.id
    const nombre = req.body.nombre
    const usuario = req.body.usuario
    const email = req.body.email
    const password = req.body.password
    const foto = req.body.foto

    // SECUENCIA SQL
    const sql = `UPDATE listaclientes SET nombre='${nombre}', usuario='${usuario}', email='${email}', password='${password}', foto='${foto}' WHERE id=${id} `

    // CONEXION A BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('cliente modificado')
    })
})

// FIN EDITAR CLIENTE



// REGISTRO:----------------------------------------------------------
// AÑADIR CLIENTES
app.post('/registro', (req, res) =>{
    // secuencia sql
    const sql = "INSERT INTO listaclientes SET ?"

    // DATOS DEL NUEVO CLIENTE
    const nuevocliente = {
        nombre: req.body.nombre,
        usuario: req.body.usuario,
        email: req.body.email,
        password: req.body.password,
        foto: req.body.foto


    }

    // CONEXIÓN A BBDD
    connection.query( sql, nuevocliente, error => {
        if(error) throw error
        console.log('Cliente creado con éxito :D')
    })
})
// FIN AÑADIR CLIENTES