# EJERCICIO HTML SIN CSS

Intentar reproducir de la forma más fiel posible la imagen proporcionada en esta carpeta.

- Se debe usar solo etiquetas HTML (es decir elementos con sus atributos)
- Obligatorio que el layout (estructura) esté generado mediante tablas
- No se puede usar CSS (ni mediente el atributo style)
