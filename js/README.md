# Entrega ejercicios JavaScript

#### 1. Entrega ejercicio Week1.js

El objetivo del ejercicio es crear un nuevo array que contenga todos los hashtags del array `tweets`, pero sin repetir

 > Nota: como mucho hay 2 hashtag en cada tweet.

#### 2. Entrega ejercicio Week2.js

Dado un array multidimensional encontrar el nombre de una persona `NameToSearch` y devolver en un array `resultSearchPhotos` los nombres de las fotografías en las que aparece.


#### 3. Entrega ejercicio Week3.js

Descargar datos de una URL externa y:
a) Generar contador de mensajes por usario.
b) Generar una lista con una estructura dada.