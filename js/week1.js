/**
 * El objetivo del ejercicio es crear un nuevo array que contenga
 * todos los hashtags del array `tweets`, pero sin repetir
 * 
 * Nota: como mucho hay 2 hashtag en cada tweet
 */
tweets = [
    'aprendiendo #javascript en Vigo',
    'empezando el segundo módulo del bootcamp!',
    'hack a boss bootcamp vigo #javascript #codinglive'
]
newArrayHidden = [];
newArrayShow = [];

numMax = 2; // Número máximo de tweets por tweet
tweetSearch_start = 0 // Inicialización en O en busca del indexOf #
tweetSearch_end = 0 // Inicialización en O en busca del primer espacio en blanco indexOf despues del primer Hashtag


for (tweet of tweets) {
    tweet = tweet + ' '; // Se añade espacio en blanco para que las palabras que están en último lugar de la frase salgan enteras
    tweet = tweet.toLowerCase(); // Poner todo en minúsculas

    //console.log('  tweet: ', tweet);

    for (i = 0; i < tweet.length; i++) {
        if (i < numMax) { // Si i es menor que el Numero Máximo de hashtags permitidos continuas 
            tweetSearch_start = tweet.indexOf('#', tweetSearch_start + i);
            tweetSearch_end = tweet.indexOf(' ', tweetSearch_start + 1);
            tweet_entero = tweet.slice(tweetSearch_start, tweetSearch_end);

            if (tweetSearch_start != -1) {  /** si tweetSearch_start es true pasamos el valor al array oculto
                                    así conseguimos que no aparezcan  comillas vacias en el 
                                    resultado por  la/las frase/sque no tiene hastags
                                 */
                newArrayHidden.push(tweet_entero); // insertamos el hashtag en el array oculto
            }
        }
    }
    // console.log('  Hashtag: ', newArrayHidden);
}

for (i = 0; i < newArrayHidden.length; i++) { // comprobamos los hashtags de newArrayHidden y hay alguno que se repite no se muestra en el resultado final
    if (newArrayShow.indexOf(newArrayHidden[i]) == -1) {
        newArrayShow.push(newArrayHidden[i]);
    }
}
console.log('\n\n ------------------------------------------------ \n\n ')
console.log('  Hashtag: ', newArrayShow);
console.log('\n\n ------------------------------------------------ \n\n ')