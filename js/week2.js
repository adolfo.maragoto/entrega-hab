/**
 * Entregable semana 2
 * 
 * Escribe el código necesario para decidir en qué
 * fotografías sale Pablo. Como resultado se debe
 * obtener un array de strings con los nombres de las
 * fotografías.
 *  
 */


function searchPhotos(arrays, nameSearched) {

    const nameFound = (nameFound => nameFound.people.indexOf(nameSearched) >= 0);
    const showPhotoName = (showPhotoName => showPhotoName.name);

    const dataReturn = arrays
        .filter ( nameFound ) // Seleccionar donde está la persona que queremos encontrar
        .map ( showPhotoName ); // Mostrar nombre de la/las fotografías seleccionadas

    return dataReturn;
}

const photos = [{
        name: 'Cumpleaños de 13',
        people: ['Maria', 'Pablo']
    },
    {
        name: 'Fiesta en la playa',
        people: ['Pablo', 'Marcos']
    },
    {
        name: 'Graduación',
        people: ['Maria', 'Lorenzo']
    },
];


const NameToSearch = 'Pablo'; // Persona a buscar 
const resultSearchPhotos = searchPhotos(photos, NameToSearch);

console.log( `\n ${NameToSearch} está en las siguientes fotos:`, resultSearchPhotos );