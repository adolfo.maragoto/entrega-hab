// https://jsonplaceholder.typicode.com/posts

// a) Generar contador de mensajes por usario
// b) Generar una lista con la siguiente estructura:
/*[
    {
        userId: <userId>,
        posts: [
            {
                title: <title>
                body: <body>     // hay que obtenerlo de la segunda petición
            },
            {
                title: <title>
                body: <body>
            },
        ]
    }

]
*/
const axios = require('axios');

const URL_BODY = 'https://jsonplaceholder.typicode.com/posts';

async function week3_exercise_A() {
    const PostReceived = await axios.get(URL_BODY);
    const POST_data = PostReceived.data; 
    const counter = {};

    for (let line_data of POST_data) {
        const currentCouncil = line_data;

        if (counter[`UserID ${currentCouncil.userId}`] != undefined) {
            counter[`UserID ${currentCouncil.userId}`]++;
        } else {
            counter[`UserID ${currentCouncil.userId}`] = 1;
        }
    }

    return counter;
}


async function week3_exercise_B() {
     // carga inicial de la URL
    const responseInitial = await axios.get(URL_BODY);
    const posts = responseInitial.data;

    const users = [];     // añadimos los datos a users

    for (let post of posts) { // comprobamos si ya exite un post
        post = await axios.get(`${URL_BODY}/${post.id}`);
        detailedPost = post.data;

        if (users[detailedPost.userId] === undefined) {
            users[detailedPost.userId] = {
                userId: detailedPost.userId,
                posts: [{
                    title: detailedPost.title,
                    body: detailedPost.body
                }]
            }   
        } else {
            users[detailedPost.userId].posts.push({ // escribimos lo que pide el ejercicio
                title: detailedPost.title,
                body: detailedPost.body
            });  
        }
    }

    return users;
}


// PRIMERA PARTE del ejercicio

week3_exercise_A().then((numMessages) => {
    console.dir(numMessages, { depth: null });
    //console.log(`Primera parte del ejercicio: ${numMessages}`);

})
.catch(error => { 
    console.log('\n !Hmmmmm! Esto no funciona. Hay que echarle un ojo a la PRIMERA parte del ejercicio \n') // en caso de que no cague da este error
});

// SEGUNDA PARTE del ejercicio

week3_exercise_B().then( users  => { 
    console.dir( users, { depth: null });
    //console.log(`Segunda parte del ejercicio: ${users}`);
    clearInterval(loading)
    })
    .catch(error => {
        console.log('\n !UPS¡ algo ha salido mal en la SEGUNDA parte del ejercicio \n')  // en caso de que no cague da este error
    });

  loading = (function() {
        var h = ['En breve la parte B del ejercicio estará ', 'C', 'CA', 'CAR', 'CARG', 'CARGA', 'CARGAD', 'CARGADO'];
        var i = 0;

        return setInterval(() => {

          i = (i > 7) ? 0 : i;
         // console.clear();
          console.log(h[i]);
          i++;
        }, 1000);
      })();