// VARIABLES

const HotelSelection = document.querySelectorAll('.button-booknow')
const insertLI = document.getElementById('ulReservas');
const DeleteAll = document.getElementById('IdButtonDeleteAll');
const asideLeft = document.getElementById('animationAside')

// FUNCIONES

const addElement = insertInReservations => {
	insertLI.appendChild(insertInReservations);
}

const createAsideReservations = asideCard => {
	localStorage.setItem('HotelReservado', JSON.stringify(asideCard));
	insertLI.innerHTML = '';
	asideCard.forEach((dataCard, i) => {

		const listLi = document.createElement('li');
		listLi.classList = 'liReservas';
		listLi.setAttribute("id", "idLiReservas");
		listLi.setAttribute('data-id', 'idHotel' + dataCard.idHotel)


		const listArticle = document.createElement('article');
		const listDivImg = document.createElement('div');

		const listImg = document.createElement('img');
		listImg.setAttribute('src', dataCard.image);

		const listDivNamePrice = document.createElement('div')

		const listTxtName = document.createElement('p');
		listTxtName.classList = 'pTitleReservas';
		listTxtName.textContent = dataCard.name;

		const listTxtPrice = document.createElement('p');
		listTxtPrice.classList = 'pTitleReservas'
		listTxtPrice.textContent = dataCard.price

		const butDeleteCard = document.createElement('button');
		butDeleteCard.classList = 'delete';
		butDeleteCard.innerHTML += "&#xe801;" // insertar icono eliminar
		//butDeleteCard.addEventListener('click', () => DeleteReservation('idHotel' + dataCard.idHotel))

		listLi.appendChild(listArticle); // crear article
		listArticle.appendChild(listDivImg); // div para insertar la imagen
		listDivImg.appendChild(listImg) // imagen del hotel
		listArticle.appendChild(listDivNamePrice); // div para insertar nombre, precio y boton
		listDivNamePrice.appendChild(listTxtName); // nombre hotel
		listDivNamePrice.appendChild(listTxtPrice); // precio de hotel
		listArticle.appendChild(butDeleteCard); // bot borrar etiqueta hotel

		addElement(listLi)

	})
}


const removeAllReservation = () => {
	const ReservationInArra = document.querySelectorAll('#idLiReservas')

	for (let i = 0; i <= ReservationInArra.length - 1; i++) {
		let OutsideArrayReservations = ReservationInArra[i]
		OutsideArrayReservations.remove(); // eliminamos todos los items en local
		localStorage.clear(); // eliminamos todos los items en localStore
	}
	asideLeft.style.display = 'none'
}

const DeleteReservation = DeleteRes => {

	// localStorage.removeItem('DeleteRes');

	// const HotelReservado = recuperarLocalStorage();
	// HotelReservado.splice('HotelReservado', 1);
	// if (HotelReservado.length == 0) {
	// 	asideLeft.style.display = 'none';
	// }
	// createAsideReservations(HotelReservado);

	DeleteRes.target.parentElement.remove();

}

const recuperarLocalStorage = () => {
	return JSON.parse(localStorage.getItem('HotelReservado'));
}

const addLocalStorage = reservation => {
	const reservations = recuperarLocalStorage() || [];
	const newReservation = [...reservations, reservation];
	createAsideReservations(newReservation);
}

const extractInfoCard = dataInfoCard => {
	const objectHotel = {
		image: dataInfoCard.querySelector('img').getAttribute('src'),
		name: dataInfoCard.querySelector('h2').innerText,
		price: dataInfoCard.querySelector('h3').textContent,
		idHotel: dataInfoCard.getAttribute('data-id')
	}
	addLocalStorage(objectHotel)
}

const sendCard = e => {
	e.preventDefault();
	// recogemos todos los datos del aside index.html
	extractInfoCard(e.target.parentElement.parentElement);
	asideLeft.style.display = 'block';
}


// LISTENERS

HotelSelection.forEach(boton => {
	//console.log(boton);
	boton.addEventListener('click', sendCard);
});
insertLI.addEventListener('click', DeleteReservation); // borrar item
DeleteAll.addEventListener('click', removeAllReservation); // eliminar todos los items