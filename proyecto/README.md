
# 1. PROYECTO NEEDOC

Basicamente el proyecto needoc es una plataforma para conectar a pacientes y doctores.
Los pacientes seleccionan la especialidad para su consulta y dejan el mensaje con los detalles de lod sintomas.

## PROYECTO NEEDOC

* [needoc](needoc)

En proyecto needoc se basa en la realización de una base de datos que soporta el proyecto (MYSQL) el Backend está realizado en Node.js y el Frontend bajo el paraguas del Framework Vue.js.

Bajo estas líneas indico los usuarios que estan en la bbdd adjunta y que contienen todos los campos para su visualización

```sh
Paciente:
log: paciente@needoc.com
pass: 1234

Doctor:
log: doctor@needoc.com
pass: 1234
```

## RECURSOS

En los recursos se pueden ver todo aquel material secundario pero que puede ser de utilidad para la comprensión del proyecto:

* [BBDD export.](recursos/02_bbdd_export)
* [Entity Relationship diagram.](recursos/01_entity_relationship_diagram)
* [Canvas.](recursos/00_canvas)
