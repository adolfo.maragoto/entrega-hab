const database = require('../../database');
const { Console } = require('winston/lib/winston/transports');

let dates = [{
        email: 'admin@needoc.es',
        //password: 1234
        password: '2b$10$CLjVfMv84.IjIyKX2obhrO8BEGnfL9QENyrzH91RSGq.NGMJtS1xq',
        role: 'admin'
    }]

// let users_bd = [];
// let doctors_bd = [];
// let messages_bd = [];
// let reply_bd = [];

// DB USERS
// const users = users_bd;

// PACIENTE & DOCTOR
// Modificar datos DOCTOR
const UpdateDataDoc =  async (id, name, surname, email, description_specialty, description_specialtygroup, url_video ) => {
    const dataUpdate = [name,surname, email, description_specialty, description_specialtygroup, url_video, id];
    console.log('UpdateDataDoc BBDD--->',dataUpdate)
    const sql='UPDATE doctors SET name=?,surname=?, email=?, description_specialty=?, description_specialtygroup=?, url_video=? WHERE id= ?'
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, Object.values(dataUpdate))
    return(rows[0])
}

// ver mensaje de la consulta seleccionada
const SeeStarDoctor = async (id_doc) => {  
    const sql = 'select avg(star) as media from reply  where id_doc_reply = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id_doc]);
    return(rows[0]);

    }
// Modificar RATING del DOCTOR
const UpdateRatingDoc =  async (id, rating) => {

    const sql='UPDATE doctors SET rating=? WHERE id= ?'
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, Object.values([rating, id]))
    console.log(id, rating, rows)
    return(rows[0])
}



// modificar datos del PACIENTE
const UpdateDataUser =  async (id, name, surname, email) => {
    const dataUpdate = [name,surname, email,id];
    const sql='UPDATE users SET name=?,surname=?,email=? WHERE id= ?'
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, Object.values(dataUpdate))
    return(rows[0])
}

// guardar los datos del PACIENTE
const saveUser = async (name, surname, age, email, password, role,  packtype, policy_number, insurance_company) => {
    console.log('saveUser BBDD-->', name, surname, age, email, password, role,  packtype, policy_number, insurance_company)

    const saveDataUserArray = [name, surname, age, email, password, role,  packtype, policy_number, insurance_company]
    const sql = 'INSERT INTO users (name, surname, age, email, password, role,  packtype, policy_number, insurance_company) VALUES (?, ?, ?, ?, SHA1(?), ?, ?, ?, ?)'
    const connection = await database.connection();
    await connection.execute(sql, Object.values(saveDataUserArray))
}

// Guardar los datos del DOCTOR
const saveDoc = async (avatar, name, surname, age, email, specialtyGroup, specialty, collegiatenumber, yearsExperience, available, password, role) => {


    const saveDataDocArray = [avatar, name, surname, age, email, specialtyGroup, specialty, collegiatenumber, yearsExperience, available, password, role]
    const sql = 'INSERT INTO doctors (avatar, name, surname, age, email, specialty_group, specialty, collegiate_number, years_experience, available, password, role) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SHA1(?), ?)'
    const connection = await database.connection();
    await connection.execute(sql, Object.values(saveDataDocArray))
    
}

// El paciente valora mensaje del DOCTOR
const saveDocStar = async (star, id) => {
    
    const dataUpdate = [star, id];
    const sql='UPDATE reply SET star=? WHERE id_reply= ?'
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, Object.values(dataUpdate))
    return(rows[0])
    
}

// Listar PACIENTES por role
async function listuser(req, res) {
    
    const sql = 'SELECT id, name, surname, age, email, role FROM users WHERE role = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, ['normal']);
    res.json(rows);
}

// listar DOCTORES por role
async function listDoc(req, res) {
    
    const sql = `SELECT id, name, surname, age, email, rating, role, avatar
    FROM doctors 
    WHERE role = ? 
    ORDER BY rating DESC`;
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, ['doctor']);
    // res.json(rows);
    return(rows)

}

// listar PACIENTE por mail
async function listUsersSearch(email) {
    const sql = 'SELECT * FROM users WHERE email = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [email]);
    return(rows[0])
}

// PACIENTE mostrar numero de mensajes de un usuario desde los últimos x días
async function countMessagesBayPack(numMess, id) {
    console.log(numMess, id)
    const sql = `SELECT COUNT(*) as number FROM messages WHERE date_send_message BETWEEN (NOW() - INTERVAL ${numMess} DAY) AND NOW() AND id_user = ${id}`;
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [numMess, id]);
    console.log(rows)
    return rows    

}



//obtener PACIENTE por id
async function getUserorDocbyID(id, role) {
    let userOrDoctor = '';

    if(role === 'normal'){
         userOrDoctor = 'SELECT * FROM users WHERE id = ?';
    }else{
        userOrDoctor = 'SELECT * FROM doctors WHERE id = ?';
   }
    const sql = userOrDoctor;
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id]);
    return(rows[0])
}

// listar DOCTOR por mail
async function listDocSearch(email) { // mostrar doctor por email
    const sql = 'SELECT * FROM doctors WHERE email = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [email]);
    return rows[0];
}


// listar DOCTORES por Especialidad
async function listDocSpecialtygroup(specialty_group) { // mostrar doctor por specialty_group

    const sql = 'SELECT * FROM doctors WHERE specialty_group = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [specialty_group]);
    return rows;
}

async function seeAndCountDuplicateSpecialtiesDoctors() { // mostrar doctor por specialty_group

    const sql = 'SELECT specialty_group, COUNT(*) numberspecialty FROM needoc.doctors  GROUP BY specialty_group HAVING numberspecialty > 1';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql);
    return rows;
}


// CONSULTA
// guardar consulta a doctor
const send_messages_doc = async (id_user, id_doc, from_message, to_message, date_send_message, specialty_group, subject, message, attached) => {

    console.log('prueba atached en bbdd', attached)

    const saveMessageArray = [id_user, id_doc, from_message, to_message, date_send_message, specialty_group, subject, message, attached]
    const sql = 'INSERT INTO messages (id_user, id_doc, from_message, to_message, date_send_message, specialty_group, subject, message, attached) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)'
    const connection = await database.connection();
    await connection.execute(sql, Object.values(saveMessageArray)) 
}

// devolver todos los mensajes -- solo para modo pruebas
async function AlllistMessages(req, res) { 
    const sql = 'SELECT * FROM messages';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql);
    //return rows;
    res.json(rows);
}

// devolver todos las consultas
const ReturnlistMessages = async () => {
    const sql = 'SELECT * FROM messages';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql);
    return(rows);
  }

// listar consultas por destinatario (mail del doctor)
const ListMessagesMailinFromMessage =  async (from_message) => {
    const sql = 'SELECT * FROM messages WHERE from_message = ?  ORDER BY date_send_message DESC';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [from_message]);
    return(rows);
    }
    
// listar consultas por destinatario (mail del doctor)
const ListMessagesMailinFromReply =  async (id_doc_reply) => {
    const sql = 'SELECT * FROM reply inner join messages on messages.id_mess = reply.id_mess  where reply.id_doc_reply = ?  ORDER BY date DESC';

    // const sql = 'SELECT * FROM reply WHERE id_doc_reply = ?  ORDER BY date DESC';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id_doc_reply]);
    return(rows);
    }

// ver mensaje consulta seleccionada
const SeeSelectedInquiry = async (id_mess) => {  
    const sql = 'SELECT * FROM messages WHERE id_mess = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id_mess]);
    return(rows[0]);
    }


// Contestación del DOCTOR a la consulta
const SaveSendReplyInquiry = async (id_mess, id_doc_reply, specialty_group, reply, date) => {
    
    const sql = 'INSERT INTO reply (id_mess, id_doc_reply, specialty_group, reply, date) VALUES (?, ?, ?, ?, ?)'
    const connection = await database.connection();
    await connection.execute(sql, Object.values([id_mess, id_doc_reply, specialty_group, reply, date]))
    
}

// Ficha del Doctor por id
const findDoctorFileinDB = async (id) => {
     
    const sql = 'SELECT * FROM doctors where id = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id]);
    return(rows[0]);
}

// buscar DOCTOR por multifiltro
async function FindDocByMultifilter (name, specialty, specialtyGroup, available){

    let sql = `SELECT id, name, surname, age, specialty_group, years_experience, available, avatar, rating, email, url_video 
            FROM doctors`;
    let count = 0;
    let parameters = [];

    if (name) {
        sql += ` WHERE name LIKE '%${name}%'`; // búsqueda por nombre completo
        //sql +=  `doctors WHERE name LIKE '%${name}%' `;  // búsqueda por nombre parcial
        parameters.push(name);
        count++;
    }

    if (specialty) {
        if (count === 0) {
            sql += ' WHERE ';
        } else {
            sql += ' AND ';
        }
        sql += ' specialty = ?';
        parameters.push(specialty);
        count++;
    }

    if (specialtyGroup) {
        if (count === 0) {
            sql += ' WHERE ';
        } else {
            sql += ' AND ';
        }
        sql += ' specialty_group = ?';
        parameters.push(specialtyGroup);
        count++;
    }

    if (available) {
        if (count === 0) {
            sql += ' WHERE ';
        } else {
            sql += ' AND ';
        }
        sql += ' available = ?';
        parameters.push(available);
        count++;
    }
    sql += ' ORDER BY rating DESC';

    const connection = await database.connection();
    const [rows] = await connection.execute(sql, parameters);
    
    return(rows) 
}


// buscar MENSAJE por multifiltro
async function FindMessagesByMultifilter (to_message, specialty_group, from_message){

    let sql = 'SELECT * FROM messages';
    let count = 0;
    let parameters = [];
    
    if (to_message) {
        sql += ' WHERE to_message = ?';
        parameters.push(to_message);
        count++;
    }

    if (specialty_group) {
        if (count === 0) {
            sql += ' WHERE ';
        } else {
            sql += ' OR ';
        }
        sql += ' specialty_group = ? ';
        parameters.push(specialty_group);
        count++;
    }

    if (from_message) {
        if (count === 0) {
            sql += ' WHERE ';
        } else {
            sql += ' OR ';
        }
        sql += ' from_message = ? ';
        parameters.push(from_message);
        count++;
    }
    sql += ' ORDER BY date_send_message DESC';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, parameters);
    
    console.log(rows)

    return(rows) 
}




 // listar preguntas y respuestas por mail
 const selectQueryAndResponse = async (id_user) => { 
    const sql = 'SELECT messages.id_mess, reply.id_reply, reply.date, reply.reply FROM messages left JOIN reply ON reply.id_mess = messages.id_mess where messages.id_user = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id_user]);
    return(rows) 
}

 // listar preguntas y repuesta por id
 const seeMyQueryAndResponse = async (id_user, id_reply) => { 
    const sql = 'SELECT * FROM messages left JOIN reply ON reply.id_mess = messages.id_mess WHERE messages.id_user = ? AND reply.id_reply = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id_user, id_reply]);
    return(rows) 
}

// comprobar si ya existe numero de colegiado
const getDocCollegiateNumber = async (collegiatenumber) => {
    const sql = 'SELECT * FROM doctors WHERE collegiate_number = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [collegiatenumber]);
    return rows[0];
}


// ESTADISTICA numero de Consultas que le envian a un Doctor por id de doctor

const NumInquiryDocByID = async (id) => {
    const sql = 'SELECT COUNT(*) as number FROM messages WHERE id_doc = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id]);

    return rows[0].number;
}

// ESTADISTICA numero de respuestas enviadas por el doctor

const NumReplyDocByID = async (id) => {
    const sql = 'SELECT COUNT(*)  as number FROM reply WHERE id_doc_reply = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id]);

    return rows[0].number;
}








module.exports = {
   
    saveDoc,
    listDoc,
    listDocSearch,
    listUsersSearch,
    saveUser,
    listuser,
    send_messages_doc,
    getDocCollegiateNumber,
    send_messages_doc,
    AlllistMessages,
    SeeSelectedInquiry,
    SaveSendReplyInquiry,
    UpdateDataUser,
    UpdateDataDoc,
    ReturnlistMessages,
    ListMessagesMailinFromMessage,
    FindDocByMultifilter,
    getUserorDocbyID,
    listDocSpecialtygroup,
    FindMessagesByMultifilter,
    selectQueryAndResponse,
    seeMyQueryAndResponse,
    saveDocStar,
    SeeStarDoctor,
    UpdateRatingDoc,
    seeAndCountDuplicateSpecialtiesDoctors,
    ListMessagesMailinFromReply,
    findDoctorFileinDB,
    NumInquiryDocByID,
    NumReplyDocByID,
    countMessagesBayPack
}