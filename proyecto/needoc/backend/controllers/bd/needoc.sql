-- users (#id, name, surname, age, email, password, rol)
-- doctors (#id, name, surname, age, email, specialtyGroup, specialty, yearsExperience, available, collegiatenumber, avatar, password, rol, -rating)
-- messages_doc (#id_mess, id_user, id_doc, date_send_message, from_message, to_message, specialtyGroup, subject, message, reply, attached)
-- reply_messages (#id_reply, -id_mess, id_doc_reply, specialtyGroup, reply, star, date  ) 


USE needoc;


SET FOREIGN_KEY_CHECKS = 0;


-- FIN BORRAR

CREATE TABLE IF NOT EXISTS  users (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
	surname VARCHAR(50),
    age VARCHAR(40),
    email VARCHAR(40) UNIQUE NOT NULL,
    password VARCHAR(256) NOT NULL,
    role VARCHAR(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS doctors (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
	surname VARCHAR(50),
    age VARCHAR(40),
    email VARCHAR(50) UNIQUE NOT NULL,
    specialty_group  VARCHAR(50) NOT NULL,
    specialty VARCHAR(50),
    years_experience DATE,
    available VARCHAR(50) ,
    collegiate_number VARCHAR(9) UNIQUE NOT NULL,
    avatar VARCHAR(500),
    password VARCHAR(256) NOT NULL,
    role VARCHAR(10) NOT NULL,
    rating DECIMAL(4,2) UNSIGNED,
    FOREIGN KEY (rating) REFERENCES reply(id_reply)
);


CREATE TABLE IF NOT EXISTS messages (
	id_mess INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_user INT UNSIGNED,
	id_doc INT UNSIGNED,
    date_send_message DATETIME,
	from_message VARCHAR(50),
    to_message VARCHAR(50),
    specialty_group VARCHAR(50),
    subject  VARCHAR(50) NOT NULL,
    message  VARCHAR(6000) NOT NULL,
    attached  VARCHAR(50)
);


CREATE TABLE IF NOT EXISTS reply (
	id_reply INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_mess INT UNSIGNED,
	FOREIGN KEY (id_mess) REFERENCES messages(id_mess),
	id_doc_reply INT UNSIGNED, 
    specialty_group VARCHAR(50),
	reply VARCHAR(50),
    star VARCHAR(50),
    date DATETIME
);


SET FOREIGN_KEY_CHECKS = 1;

-- se modificar un campo para que tenga un numero de hasta cuatro cifras con dos decimales

alter table doctors modify column rating decimal(4,2);

-- se añade un usuario de cada tipo:

SELECT * FROM messages left JOIN reply ON reply.id_mess = messages.id_mess where reply.id_mess = '82';

-- administrador
INSERT INTO users (name, surname, age, email, password, role)
 VALUES ('Adolfo', 'Maragoto', '21', 'admin@needoc.com', SHA1('1234'), 'admin'); 

-- paciente
INSERT INTO users (name, surname, age, email, password, role)
 VALUES ('Federico', 'Sanlucar Rigueria', '34', 'federico@needoc.com', SHA1('1234'), 'normal'); 
 
-- doctor 
INSERT INTO doctors (name, surname, age, email, specialty_group, specialty, years_experience, available, collegiate_number, avatar, password, role)
 VALUES ('Rogelio', 'Bodeguin Sandiego', '37', 'doc3@needoc.com', 'Pediatría', 'Pediatra', '2010-12-05', 'alta', '233584289', 'imagen.jpg', SHA1('1234'), 'doctor'); 


 SELECT * FROM users;
 
-- SELECT * FROM users_bd WHERE role LIKE 'n%' -- mostrar el que comienza por P

SELECT * FROM doctors WHERE name LIKE 'P%';

   