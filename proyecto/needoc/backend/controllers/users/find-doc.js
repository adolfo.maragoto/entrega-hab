const { FindDocByMultifilter, listDoc, seeAndCountDuplicateSpecialtiesDoctors } = require('../bd/bd');

const winston = require('winston');

//logs
const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.json(),
    defaultMeta: {
        service: 'user-service'
    },
    transports: [
        new winston.transports.File({
            filename: '../logs/error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: '../logs/info.log',
            level: 'info'
        }),
        new winston.transports.File({
            filename: '../logs/combined.log'
        }),
        new winston.transports.Console({
            format: winston.format.simple()
        })
    ]
});

const listFind = async (req, res) => { // recibe dos parametros: ruta y la funcion req res

    const { name, specialty, specialtyGroup, available } = req.query

    try {
        let resultFind = await FindDocByMultifilter(name, specialty, specialtyGroup, available)
        if (resultFind.length === 0) {
            const response = {
                'code': 201,
                'error': 'No hay resultados'
            }
             // lo paso como array para que los coja en front ya que solitita un array
            resultFind = [response];
        }
        res.status(201).send(resultFind)

    } catch (e) {
        res.status(400).send();
        logger.error('Error en el envio de la consulta');
        return;
    }
}

const seeDoctorsAndAtatistics = async (req, res) => {
    // buscamos listado de doctores 
    const resultlistDocs = await listDoc()

    // recogemos datos de cuantos doctores hay por especialidad
    const resultDuplicateSpecialties = await seeAndCountDuplicateSpecialtiesDoctors();

    // pasamos los datos
    const response =  {
        'specialties': resultDuplicateSpecialties,
        'doctors': resultlistDocs,
    }

    res.send(response)
}

module.exports = {
    listFind,
    seeDoctorsAndAtatistics
}