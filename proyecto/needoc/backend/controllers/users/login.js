const jwt = require('jsonwebtoken');
const { listUsersSearch, listDocSearch } = require('../bd/bd');

const database = require('../../database')
const winston = require('winston');

//logs
const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.json(),
    defaultMeta: {
        service: 'user-service'
    },
    transports: [
        new winston.transports.File({
            filename: '../logs/error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: '../logs/info.log',
            level: 'info'
        }),
        new winston.transports.File({
            filename: '../logs/combined.log'
        }),
        new winston.transports.Console({
            format: winston.format.simple()
        })
    ]
});

async function loginUserDB(email, password, role) {

    let userOrDoctor = '';

    if (role === 'doctor') {
        userOrDoctor = 'SELECT id, email, role FROM doctors WHERE email = ? AND password = SHA1(?)';
    } else {
        userOrDoctor = 'SELECT id, email, role FROM users WHERE email = ? AND password = SHA1(?)';
    }

    try {
        const sql = userOrDoctor;
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, [email, password]);

        if (rows[0]) {
            const tokenPayload = {
                'id': rows[0].id,
                'role': rows[0].role,
                'email': rows[0].email
            };

            const token = jwt.sign(tokenPayload, process.env.SECRET, {
                expiresIn: '365d'
            });

            return (token);
        } else {
            const response = {
                'code': 201,
                'error': 'Login incorrecto'
            }
            return (response)
        }
    } catch (exception) {
        logger.error(exception)
        return {
            'code': 500,
            'description': exception.toString()
        };
    }

}
const login = async (req, res) => {
    try {
        const { email, password, role } = req.body;
        
        console.log('llegada datos -->', email, password, role)

        if(role == 'normal'){
            responseuserdoc = await listUsersSearch(email)

        } else {
            responseuserdoc = await listDocSearch(email)
        }

        // console.log('paciente o doctor-->', responseuserdoc)

        const response = await loginUserDB(email, password, role)



        const data = {
            email: email,
            token:response,
            role: role,
            id: responseuserdoc.id,
            name: responseuserdoc.name,
            packtype: responseuserdoc.packtype
        }

        res.json(data);

    } catch (exception) {
        logger.error(exception)
        return {
            'code': 500,
            'description': exception.toString()
        };
    }
};

module.exports = {
    login
}