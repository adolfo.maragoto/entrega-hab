const moment = require('moment')
const winston = require('winston');

const { ListMessagesMailinFromReply, UpdateRatingDoc, SeeStarDoctor, saveDocStar, seeMyQueryAndResponse, selectQueryAndResponse, FindMessagesByMultifilter, getUserorDocbyID, ListMessagesMailinFromMessage, ReturnlistMessages, listDocSpecialtygroup, send_messages_doc, SeeSelectedInquiry, SaveSendReplyInquiry, listDocSearch } = require('../bd/bd');
const { sendEmailMedicalInquiry} = require('../emails/sendEmails');


//logs
const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
      new winston.transports.File({ filename: '../logs/error.log', level: 'error' }),
      new winston.transports.File({ filename: '../logs/info.log', level: 'info' }),
      new winston.transports.File({ filename: '../logs/combined.log' }),
      new winston.transports.Console({
        format: winston.format.simple()
      })
    ]
  }); 


// PACIENTE: mostrar las consultas enviadas por el paciente
const listMessages = async (req, res) => {
  // recibimo el mail del Paciente
  const { email } = req.auth; 
  const { id } = req.auth; 

  let result = await ReturnlistMessages();
  
  if(req.auth.role === 'normal') {
    // buscamos todos los mensajes que tengan mi email en from_message
    result = await ListMessagesMailinFromMessage(email)

  } else if(req.auth.role === 'doctor') {
    // buscamos todos los mensajes que tengan mi email en from_message
    result = await ListMessagesMailinFromReply(id)
  }


  let nresult=result

  const data = {
    'number_messages':result.length,
    data: nresult
  }
  res.json(data);
}

// DOCTOR: mostrar las consultas respondidas por el Doctor
const listMessagesDoctors = async (req, res) => {
  const { id_doc } = req.auth; 

  let result = await ReturnlistMessages();
  if(req.auth.role === 'normal') {
    // buscamos todos los mensajes que tengan mi email en from_message
    result = await ListMessagesMailinFromReply(id_doc)
  }
  let nresult=result

  const data = {
    'number_messages':result.length,
    data: nresult
  }
  res.json(data);
}





// ENVIAR CONSULTA
const inquirySent = async (req, res) => {
  
  //recogemos datos de body y auth
  const { subject, message, specialtyGroup, attached, email_doc } = req.body;

  const id_user = req.auth.id;
  const role = req.auth.role;
  
   // obtenemos el email del Paciente
  let result = await getUserorDocbyID(id_user, role)
  const from =  result.email;

  const to = email_doc 
  let id_doc = 0; 
  const reply = ''; 


  if(!email_doc && !specialtyGroup){
    const response = {
      'code': 201,
      'message': 'A quien le quieres enviar tu consulta'
    }
    logger.info('A quién le quieres enviar tu consulta');
    res.send(response);

  }

  if(email_doc.length > 0){
    let getDataDoctor = await listDocSearch(email_doc)

    if(getDataDoctor === undefined){ // buscamos que exista el doctor

    const response = {
      'code': 201,
      'message': 'El mail introducido no existe'
    }
    logger.info('El mail introducido no existe');
    res.send(response);

    }
    // muestra el id del doctor 
      id_doc = getDataDoctor.id;  
  }

  if(specialtyGroup.length > 0){ // buscamos que algun doctor con esta especialidad
    let getDataDoctorspecialtyGroup = await listDocSpecialtygroup(specialtyGroup);
    console.log(getDataDoctorspecialtyGroup.length)
  }

// fecha y hora del mensaje
  let datetime = moment().format( 'YYYY-MM-DD  HH:mm:ss.000' );

 // guardar el mensaje en la bbdd
// send_messages_doc(id_user, id_doc, from, to, datetime, specialtyGroup, subject, message, reply, attached)
send_messages_doc(id_user, id_doc, from, to, datetime, specialtyGroup, subject, message, attached)

// Envio mail
    if(id_doc !== 0){
      sendEmailMedicalInquiry(from, to);
        logger.info('mail enviado: Tienes una consulta nueva');
    }
// fin envio mail

    const response = {
      'code': 200,
      'message': 'Tu mensaje a sido enviado correctamente'
    }
 
res.send(response);

}



// MOSTRAR BANDEJA DE ENTRADA
const viewInbox = async (req, res) => {
  const id = req.auth.id;
  const role = req.auth.role;

  // traemos los datos del paciente o doctor (la busqueda se define en la bbdd)
  // para hacer la búsqueda de los mensajes con los datos necesarios
  let resultMessages = '';
  let resultUserOrDoc = await getUserorDocbyID(id, role);

  let nresult=[];

  if (role === "doctor") {
    // mostramos el email y la especialidad del doctor
    let resultDoc = {
      'email': resultUserOrDoc.email,
      'specialty_group': resultUserOrDoc.specialty_group,
    };

    const email = resultDoc.email;
    const specialty_group = resultDoc.specialty_group;

    // hacemos la busqueda por email o especialidad del doctor
    resultMessages = await FindMessagesByMultifilter(email, specialty_group);

    for(i in resultMessages){
      nresult.push({
          'id_mess': resultMessages[i].id_mess,
          'date_send_message': resultMessages[i].date_send_message,
          'subject': resultMessages[i].subject
      });

  }
    
  }
  if (role === "normal") {
    console.log('paciente')

    email = "0";
    specialty_group = "0";

    let resultMessages = await selectQueryAndResponse(id);
    //res.json(salida)

    //resultMessages = await FindMessagesByMultifilter(email, specialty_group, from_message);
    for (i in resultMessages) {
      if (resultMessages[i].date != null) {
        nresult.push({
          id_reply: resultMessages[i].id_reply,
          id_message: resultMessages[i].id_mess,
          date_send_message: resultMessages[i].date,
          subject: resultMessages[i].reply,
        });
      }
    }
  }         

  let result = {
    'number_messages':nresult.length,
     data : nresult
  }
  if(nresult.length === 0) {
    result = {
      'error': 201,
      'mensaje': 'no tienes mensajes'
    }
  }
  res.json(result);

};


// DOCTOR VER CONSULTA SELECCIONADA Y CONTESTARLA SELECCIONADA POR -> /:id
const viewSelectedMessagePOST = async (req, res) => {
  const { reply, star } = req.body;

  const id_mess  = req.params.id_mess 
  const id = req.auth.id
  const role_token = req.auth.role

  //encontramos datos del doctor o paciente
  const persona = await getUserorDocbyID(id, role_token)
  const personaEmail = persona.email
  const personaSpecialtyGroup = persona.specialty_group
  const personaId = persona.id

  
  if (role_token === 'doctor') {
    // buscar consulta en messages
    const SeeSelectedInquirybyID = await SeeSelectedInquiry(id_mess);


    if (SeeSelectedInquirybyID === undefined) {
      const response = {
        code: 404,
        error: "Este mesaje no existe",
      };
      logger.info("No existe el mensaje");
      res.send(response);
    }

    const inquiryEmailTo = SeeSelectedInquirybyID.to_message;
    const inquiryEmailSpecialtyGroup = SeeSelectedInquirybyID.specialty_group;

    if ((inquiryEmailTo === id_mess && inquiryEmailTo === personaEmail) ||
      inquiryEmailSpecialtyGroup === personaSpecialtyGroup) {

      // fecha y hora del mensaje
      let date = moment().format("YYYY-MM-DD  HH:mm:ss.000");
      // enviar datos a bbdd
      SaveSendReplyInquiry(id_mess, id, personaSpecialtyGroup, reply, date);
      
      res.send([SeeSelectedInquirybyID]);

    } else {
      const response = {
        code: 403,
        error: "No tienes permiso para acceder aquí",
      };
      logger.info("Error en el envio de la respuesta");

      res.send(response);
    }
  }
  
  if (role_token === 'normal'){
    // buscar consulta en messages
    const SeeSelectedInquirybyID = await seeMyQueryAndResponse(personaId, id_mess);

        
    nresult=[]
    for (i in SeeSelectedInquirybyID) {
      // recoger por id el mail del doctor que contestó el mensaje
      let role_doctor = 'doctor'
      let id = SeeSelectedInquirybyID[i]. id_doc_reply
      const persona = await getUserorDocbyID(id, role_doctor)

        nresult.push({
          message: {
            id_message: SeeSelectedInquirybyID[i].id_mess,
            date_send_message: SeeSelectedInquirybyID[i].date,
            subject: SeeSelectedInquirybyID[i].subject,
            message: SeeSelectedInquirybyID[i].message,
          },
          reply: {
            id_reply: SeeSelectedInquirybyID[i].id_reply,
            to_message: persona.email,
            reply: SeeSelectedInquirybyID[i].reply,
            star: SeeSelectedInquirybyID[i].star
          }
          
        });

        //valorar respuesta
        const id_reply = nresult[0].reply.id_reply
        

        saveDocStar(star, id_reply)

        // MUESTRA VALORACION MEDIA DEL DOCTOR
        const avgRate = await SeeStarDoctor(id)
        // console.log('dataDoctorRatig-->',avgRate['media'])

        // añadimos la valoracion al doctor datos al doctor
          UpdateRatingDoc(id, avgRate['media'])
      }
    
    res.json(nresult)
  }
  
}


module.exports = {
  inquirySent,
  listMessages,
  viewSelectedMessagePOST,
  viewInbox,
  listMessagesDoctors
};