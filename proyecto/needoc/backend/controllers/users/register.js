const { listDocSearch, listUsersSearch, saveDoc, saveUser, getDocCollegiateNumber } = require('../bd/bd');

const { sendEmailRegister } = require('../emails/sendEmails')
const winston = require('winston');

//logs
const logger = winston.createLogger({
  level: 'debug',
  format: winston.format.json(),
  defaultMeta: {
    service: 'user-service'
  },
  transports: [
    new winston.transports.File({
      filename: '../logs/error.log',
      level: 'error'
    }),
    new winston.transports.File({
      filename: '../logs/info.log',
      level: 'info'
    }),
    new winston.transports.File({
      filename: '../logs/combined.log'
    }),
    new winston.transports.Console({
      format: winston.format.simple()
    })
  ]
});

const normalizeText = (name) => {
  return name
    .trim()
    .split(' ')
    .filter(item => item.length > 0)
    .map(word => `${word[0].toUpperCase()}${word.slice(1)}`)
    .join(' ');
}


const register = async (req, res, next) => {
  const {
    name,
    surname,
    age,
    password,
    role,
    packtype, 
    policy_number,
    insurance_company

  } = req.body;
  
  const emailbody = req.body.email
  const email = emailbody.trim().replace(/ /g, "") // sacar espacios en blanco

  // Registramos los datos del paciente o doctor

  try{
  // DOCTOR----------------------------------
  // DOCTOR COMPROBAR Y GUARDAR DATOS
      if (role === 'doctor') {

        const { avatar, specialtyGroup, specialty, yearsExperience, available, collegiatenumber } = req.body;
        
        const rating = 0;
        
        const existCollegiateNumber = await getDocCollegiateNumber(collegiatenumber)
        if(existCollegiateNumber != undefined){
          const existData = new Error('Vaya! El número de colegiado introducido ya existe en nuestra base de datos');
          existData.status = 201;
          next(existData);
          return;
      }

        const existEmail = await listDocSearch(email)
        if(existEmail != undefined){
          const existUser = new Error('Este Email ya existe');
          existUser.status = 201;
          
          next(existUser);
          return;
      }

      
        // guardamos los datos del doctor
        saveDoc(avatar, normalizeText(name), normalizeText(surname), age, email, normalizeText(specialtyGroup), normalizeText(specialty), collegiatenumber, yearsExperience, normalizeText(available), password, role, rating);
        
        // Envio de email de confirmación de registro Doctor
        sendEmailRegister(name, email);

        res.status(201).send();
        return
      }
// PACIENTE -----------------------------------------
// PACIENTE COMPROBAR Y GUARDAR DATOS
      const existEmail = await listUsersSearch(email)
      if(existEmail != undefined){
        const existUser = new Error('Este Email ya existe');
            existUser.status = 201;
           
            next(existUser);
            return;
        }
        // guardamos los datos del paciente
        console.log('saveUser back-->', normalizeText(name), normalizeText(surname), age, email, password, role, packtype, policy_number, insurance_company)

        saveUser(normalizeText(name), normalizeText(surname), age, email, password, role, packtype, policy_number, insurance_company);

        // Envio de email de confirmación de registro Paciente

        sendEmailRegister(name, email);
      
        res.status(201).send();

  }catch (error) {
          logger.error(error.toString())
          return {
              'code': 500,
              'description': error.toString()
          };
      }
 }

module.exports = {
  register
}