const { countMessagesBayPack, NumReplyDocByID, NumInquiryDocByID, listUsersSearch, listDocSearch, UpdateDataUser, UpdateDataDoc, findDoctorFileinDB } = require('../bd/bd');
const winston = require('winston');

//logs
const logger = winston.createLogger({
  level: 'debug',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: '../logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: '../logs/info.log', level: 'info' }),
    new winston.transports.File({ filename: '../logs/combined.log' }),
    new winston.transports.Console({
      format: winston.format.simple()
    })
  ]
});

const normalizeText = (name) => {
  return name
      .trim()
      .split(' ')
      .filter(item => item.length > 0)
      .map(word => `${word[0].toUpperCase()}${word.slice(1)}`)
      .join(' ');
}


// MOSTRAR USUARIO POR ID DEL PARAM
const showUserAndDoc = async (req, res) => {   
    const { id } = req.params;
    const { email } = req.auth
  
    // Si en el token es doctor entonces pasamos buscamos en la bbdd del DOCTOR
    // sino en la de PACIENTES
    if (req.auth.role === 'doctor') {
       bdatesUser = await listDocSearch(email);
    }else {
       bdatesUser = await listUsersSearch(email);
    }
    
    if (id.length <= 0) {
      //Comparamos los id's para que tengas permisos de modificación solo para tu usuario
      res.status(403).send("Vaya! parece que no estás en en el lugar correcto.");
    return;
      ;
    }
    if (parseInt(id) !== req.auth.id) {
      //Comparamos los id's para que tengas permisos de modificación solo para tu usuario
      res.status(403) .send("Hmmmm! No tienes permiso para acceder este usuario");
      return;
    }
    // console.log('UPDATE.JS result', bdatesUser)

    result = [bdatesUser]
    res.json(result)

}

// modificar datos Paciente
const updateUser = async (req, res) => {   
  const { id } = req.params;

  const { email } = req.auth
  const { name, surname } = req.body;
  console.log('base de datos', name, surname)
  const emailTrim = email.trim().replace(/ /g, "") // sacar espacios en blanco

  // Si en el token es paciente entonces buscamos en la bbdd de doctor
  // sino en la de pacientes
  
    const bdatesUser = await  listUsersSearch(email);
    const bdatesUserID = bdatesUser.id
    
  if (bdatesUserID.length < 0) {
    //Comparamos los id's para que tengas permisos de modificación solo para tu usuario
    res.status(403)
    .send("Vaya! parece que no estás en el lugar correcto.");
  return;
  }
  if (parseInt(id) !== req.auth.id) {
    //Comparamos los id's para que tengas permisos de modificación solo para tu usuario
    res.status(403)
      .send("Hmmmm! No tienes permiso para acceder este usuario");
    return;
  }
  

  UpdateDataUser(bdatesUserID, normalizeText(name), normalizeText(surname), emailTrim)
  
  const response = {
    'name': bdatesUser.name,
    'surname': bdatesUser.surname,
    'age': bdatesUser.age,
    'email': bdatesUser.email
}
  res.json(response)

}
const showDoctorfile = async (req, res) =>{
  
  const { id } = req.params;

  try{
    // buscamos la ficha del doctor
    let result = await findDoctorFileinDB(id)
    // numero de consultas recibidas
    let resultInqStat = await NumInquiryDocByID(id) 
    // numero de consultas respondidas
    let resultReplyStat = await NumReplyDocByID(id) 


    //sacamos el resulado
    let nresult = result

    const data = {
      filedoctor: nresult,
      statisticsInquiry: resultInqStat,
      statisticsReply:  resultReplyStat
    }
    console.log('result en update.js', data)


  res.json(data);

  }catch(e){
    const response = {
        code: 201,
        error: "Este doctor no existe",
      };

    logger.error("error peticion ficha doctor: ", e); 
    res.send(response);
    }
  
}

// MODIFICAR DOCTOR
const updateDoc = async (req, res) => {   
  const { id } = req.params;
  const { email } = req.auth
  const { name, surname, description_specialty, description_specialtygroup, url_video } = req.body;
  const emailTrim = email.trim().replace(/ /g, "") // sacar espacios en blanco

  // Si en el token es doctor entonces pasamos buscamos en la bbdd de doctor
  // sino en la de usuarios normales
  
    const bdatesDoctor = await  listDocSearch(email);
    const bdatesDoctorID = bdatesDoctor.id
    
    if (!bdatesDoctorID) {
      res.status(403).send("Este usuario no existe");
      return;
    }
    if (bdatesDoctorID.length < 0) {
    //Comparamos los id's para que tengas permisos de modificación solo para tu usuario
    res.status(403).send("Vaya! parece que no estáse en el lugar correcto.");
  return;
  }
  if (parseInt(id) !== req.auth.id) {
    //Comparamos los id's para que tengas permisos de modificación solo para tu usuario
    res.status(403) .send("Hmmmm! No tienes permiso para acceder este usuario");
    return;
  }
  
  UpdateDataDoc(bdatesDoctorID, normalizeText(name), normalizeText(surname), emailTrim,  description_specialty, description_specialtygroup, url_video )

  const response = {
    'name': bdatesDoctor.name,
    'surname': bdatesDoctor.surname,
    'age': bdatesDoctor.age,
    'email': bdatesDoctor.email,
    'specialty_group': bdatesDoctor.specialty_group,
    'specialty': bdatesDoctor.specialty,
    'years_experience': bdatesDoctor.years_experience,
    'available': bdatesDoctor.available,
    'rating': bdatesDoctor.rating,
    'avatar': bdatesDoctor.avatar,
    'description_specialty': bdatesDoctor.description_specialty,
    'description_specialtygroup': bdatesDoctor.description_specialtygroup,
    'url_video': bdatesDoctor.url_video
}


  res.status(201).json(response)

}

// numero de mensajes que me quedan por enviar según pack contratado
const numeberMessagesByPack = async (req, res) => {  
  
  // numMess es el tipo de pack contratado
  const { typePack, id } = req.query;
  
  // según el pack que tenga le asignamos unos días
  // estos días serán los que tiene para envir x mensajes
  let numDays =''
  let status = ''
  if(typePack == 'Alta'){
    numDays = 30;
    packType = 'Gold Pro'
  }
  if(typePack == 'Media'){ 
    numDays = 15
    packType = 'Silver'

  }
  if(typePack == 'Baja'){ 
    numDays = 7
    packType = 'Basic'
  }
  

   let  salida = await countMessagesBayPack(numDays, parseInt(id))
   
   if(salida[0].number <= numDays){
     warningPack = ` ${numDays - salida[0].number}`;
     status = 1;
   }
   else{
    warningPack = 'Has cubierto tu cupo de mensajes'
    status = 0;
   }

   const response = [{

    'packType': packType,
    'totalnumbermessages': salida[0].number,
    'numberdays': numDays,
    'warningPack': warningPack,
    'status': status

  }]
   console.log(response)

   res.send(response[0])
}

module.exports = {
    showUserAndDoc,
    updateUser,
    updateDoc,
    showDoctorfile,
    numeberMessagesByPack
}