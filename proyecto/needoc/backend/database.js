const mysql = require('mysql2/promise');

async function connection() {
    return await mysql.createConnection({
        host: 'localhost',
        user: 'needoc',
        password: 'needoc',
        database: 'needoc',
        timezone: 'Z'
    });
}

module.exports = {
    connection
};