require('dotenv').config();

const jwt = require('jsonwebtoken');

const isAuthenticated = (req, res, next) => {
    const { authorization } = req.headers;

    // console.log('\n\n authorization AUTH.JS - linea 7',req.headers, '\n\n')
    // console.log('\n\n authorization AUTH.JS - linea 7',req.headers.authorization, '\n\n')

    try {
        const decodedToken = jwt.verify(authorization, process.env.SECRET);
        req.auth = decodedToken;
        console.log('role-->',req.auth.role)

    } catch(e) {
        const authError = new Error('invalid token');
        authError.status = 401;
        return next(authError);
    }

    next();
}

const isAdmin = (req, res, next) => {
    if (!req.auth || req.auth.role !== 'admin') {
        const authError = new Error('not-authorized');
        authError.status = 403;
        return next(authError);
    }

    next();
}


module.exports = {
    isAdmin,
    isAuthenticated
};