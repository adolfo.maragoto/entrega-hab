// Proyecto needoc
// Proyecto 01101110 01100101 01100101 01100100 01101111 01100011 
                                                                
require('dotenv').config();

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const winston = require('winston');

const { listuser, listDoc, AlllistMessages } = require('./controllers/bd/bd'); // base de datos
const { register } = require('./controllers/users/register'); // resgistro
const { login } = require('./controllers/users/login'); // login 
const { listFind, seeDoctorsAndAtatistics } = require('./controllers/users/find-doc'); // listar busqueda
const { numeberMessagesByPack, showUserAndDoc, updateUser, updateDoc, showDoctorfile } = require('./controllers/users/update') // modificar mostrar
const { isAuthenticated /*, isAdmin*/ } = require('./middlewares/auth') // autenticación
const { inquirySent, listMessages, viewSelectedMessagePOST, viewInbox } = require('./controllers/users/messages') // enviar mensaje

//logs
const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.json(),
    defaultMeta: {
        service: 'user-service'
    },
    transports: [
        new winston.transports.File({
            filename: '../logs/error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: '../logs/info.log',
            level: 'info'
        }),
        new winston.transports.File({
            filename: '../logs/combined.log'
        }),
        new winston.transports.Console({
            format: winston.format.simple()
        })
    ]
});

 const port = process.env.PORT;
// const port  = 3050

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());  

// GENERAL
app.get('/', seeDoctorsAndAtatistics );       // ✓✓  ✌ listado basido doctores y estadisticas especialidades
app.get('/datos', numeberMessagesByPack );                // ✓✓  ✌listar todas las consultas de bbdd


// USERS
app.post('/user', register );                             // ✓✓ ✌ Registro
app.post('/login', login);                                // ✓✓ ✌ Login
app.get('/user/:id', isAuthenticated, showUserAndDoc );   // ✓✓ ✌ Mi Perfil: obtener datos del usuario
app.patch('/user/:id', isAuthenticated, updateUser );     // ✓✓  ✌Mi Perfil: modificar mis datos (rol: usuario)

// DOCTORS
app.post('/doctors', isAuthenticated, listFind );          // ✓✓  ✌ BUSCADOR: buscar doctor por: name specialty available
app.get('/doctor/:id', isAuthenticated, showUserAndDoc );  // ✓✓  ✌  Mi Perfil: obtener datos del doctor/doctora
app.patch('/doctor/:id', isAuthenticated, updateDoc );     // ✓✓  ✌  Mi Perfil: modificar datos (rol: doctor)

app.get('/doctor/file/:id', /*isAuthenticated,*/ showDoctorfile );     // ✓✓  ✌  Mi Perfil: modificar datos (rol: doctor)


// MESSAGES
app.post('/send', isAuthenticated, inquirySent);                           // ✓✓  ✌PACIENTE -> enviar consulta a doctor
app.get('/allmessages', isAuthenticated, AlllistMessages );                // ✓✓  ✌listar todas las consultas de bbdd
app.get('/sentmail', isAuthenticated, listMessages );                      // ✓✓  ✌PACIENTE -> listar datos basicos de las consultas del PACIENTE al Doctor 
app.get('/inbox', isAuthenticated, viewInbox );                            // ✓✓  ✌DOCTOR Y PACIENTE -> muestra una lista de mensajes recibidos tanto el doctor como paciente
 
app.post('/inbox/:id_mess', isAuthenticated, viewSelectedMessagePOST );    // ✓✓  ✌DOCTOR Y PACIENTE:
                                                                                  // DOCTOR ve mensaje ampliado y responde consulta
                                                                                  // PACIENTE ve respuesta de doctor y la valora 
                                                                                    // este valor es qeu hace el rating del doctor       


app.use((error, req, res, next) => {
    res.status(error.status || 500)
        .send({status: 'error', message: error.message})
});


// PUERTO
app.listen(port, () => {
    logger.info(`Server listening on port ${port}`)
});