
// IMPORTANDO COSAS DEL TOKEN
const config = require('./config')

const jwt = require('jsonwebtoken')

// Declarando cosas que instalé

const cors = require('cors')
const bodyParser = require('body-parser')
const mysql = require('mysql')
const express = require('express')
const app = express()

// Cosas que usa la APP

app.use(cors())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.set('llave', config.llave) // traemos las llaves de token


// conexión a la BBDD
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'notas'
})

// realizando conexión a BBDD
connection.connect(error => {
    if(error) throw error
    console.log('DATABASE CONECTADA :)')
})

// PUERTO DE CONEXIÓN DEL SERVICIO
const PORT = 3050

// CONEXIÓN DEL SERVICIO
app.listen(PORT, () => console.log('API CONECTADA :)'))



// RECOGER TODOS LOS CLIETES DE LA BASE
app.get('/', (req, res) => {
    res.send('Hola')
})

// RECOGIENDO TODOS LOS CLIENTES DE LA BASE DE DATOS
app.get('/clientes', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM lista_cliente'

    // conexion a BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay clientes que mostrar. :(')
        }
    })
})

// AÑADIR CLIENTES A LA BBDD
app.post('/add',(req, res) => {

    // SECUENCIA SQL
    const sql = "INSERT INTO lista_cliente SET ?"

    // OBJETO DE DATOS DEL NUEVO CLIENTE
    const nuevoCliente = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        ciudad: req.body.ciudad,
        empresa: req.body.empresa
    }

    // CONEXIÓN A BBDD
    connection.query( sql, nuevoCliente, error => {
        if(error) throw error
        console.log('Cliente credo con éxito :D')
    })

})

// ACTUALIZANDO LA BBDD
app.put('/update/:id',(req,res) =>  {

    // DATOS QUE RECIBIMOS
    const id = req.params.id
    const nombre = req.body.nombre
    const apellido = req.body.apellido
    const ciudad = req.body.ciudad
    const empresa = req.body.empresa

    // SECUENCIA SQL
    const sql = `UPDATE lista_cliente SET nombre='${nombre}', apellido='${apellido}', ciudad='${ciudad}', empresa='${empresa}'  WHERE id=${id} `

    // CONEXION A BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('cliente actualizado con éxito')
    })
})


// BORRANDO CLIENTES DE LA BBDD
app.delete('/delete/:id', (req, res) => {
    
    //DATOS QUE LEGAN DE LA VISTA
    const id = req.params.id

    // SECUENCIA SQL
    const sql = `DELETE FROM lista_cliente WHERE id=${id}`

    // CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('cliente borrado')
    })
})



// AUTENTICACION
// AUTH DE LA API
// función que recoge los datos del usuario y que la bbdd nos lo valide

app.post('/auth', (req, res) => {

    // datos que recibo
    const user = req.body.user
    const password = req.body.password

    // secuencia sql
    const sql =`SELECT * FROM usuarios WHERE user='${user}' AND password='${password}'`

    // conexion a la bbdd
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            const payload = {
                check: true
            }
            // guardando si es admin o no
            let admin = null
            if(results[0].isAdmin === 1){
                admin = true
            }else {
                admin = false
            }

            // GUARDANDO EL NOMBRE DEL USUARIO
            let user =''
            user = results[0].user

            // token
            const token = jwt.sign(payload, app.get('llave'), {
              expiresIn: '2 days'  
            })
            res.json({
                mensaje: 'Autenticación completada con éxito',
                token: token,
                admin: admin,
                user: user
            })
        } else {
            console.log('Datos incorrectos' )
        }
    })
})