import jwt from 'jwt-decode'
import axios from 'axios'

// guardamos endpoint de la api
const ENDPOINT = 'http://localhost:3050'

export function login(email, password, role) {
       return axios.post(`${ENDPOINT}/login`, {
            email: email,
            password: password,
            role: role
        })
        .then(function(response){
            console.log(response)
            self.errorBack = response.data.message
            // guardo el token
            setAuthToken(response.data.token)
            // guardo el rol
            setIsAdmin(response.data.role)
            // guardo el nombre de user
            setName(response.data.name)
            // guardo el id del usuario
            setID(response.data.id)

            if(response.data.role == 'normal'){
                // guardo el id del usuario
                setPACK(response.data.packtype)
            }
        })
        .catch(function(error){
            console.log(error)
        })
}

// función para guardar en localstorage el jsonwebtoken
// y confirmar qu la persona está logeada

// buscar en  consola APLICATION, Local Storage
export function setAuthToken(token){
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    localStorage.setItem('AUTH_TOKEN_KEY', token)
}

// función para recuperar el token desde localstorage
export function getAuthToken(){
    return localStorage.getItem('AUTH_TOKEN_KEY')
}

// FUNCIÓN PARA CONSEGUIR LA FECHA DE CADUCIDAD DEL TOKEN
export function tokenExpiration(encodedToken) {
    let token = jwt(encodedToken)
    if(!token.exp) {
        return null
    }
    let date = new Date(0)
    date.setUTCSeconds(token.exp)
    return date
}

// FUNCIÓN QUE COMPRUEBA SI LA PERSONA ESTÁ LOGUEADA Y SU TOKEN ES VALIDO
export function isLoggedIn(){
    let authToken = getAuthToken()
    return !!authToken && !isExpired(authToken)
}

// FUNCIÓN QUE COMPRUEBA SI EL TOKEN ESTÁ POCHO O NO
export function isExpired(token){
    let expirationDate = tokenExpiration(token)
    return expirationDate < new Date()
}


// función guardar admin en localstorage
// admin valor recuperado de la base de datos
export function setIsAdmin(role) {
    localStorage.setItem('ROLE', role) // lo guardamos en localstorage
}

//FUNCION PARA RECUPERAR EL ADMIN DE LOCALSTORAGE
export function getIsAdmin() {
    return localStorage.getItem('ROLE')
}

// función guardar pack en localstorage
// admin valor recuperado de la base de datos
export function setIsPack(pack) {
    localStorage.setItem('PACK', pack) // lo guardamos en localstorage
}

//FUNCION PARA RECUPERAR EL PACK DE LOCALSTORAGE
export function getIsPack() {
    return localStorage.getItem('PACK')
}

// FUNCION PARA SABER SI ES ADMIN O NO
export function checkIsAdmin(){
    let role = null
    let admin = getIsAdmin()
    
    if(admin === 'true'){
        role = true
        console.log(admin)
    } else {
        role = false
        console.log(admin)
    }
    return role
}

// FUNCIÓN PARA SABER SI ES PACIENTE
export function checkIsPatient(){
    let role = null
    let admin = getIsAdmin()
    
    if(admin === 'normal'){
        role = true
        console.log(admin)
    } else {
        role = false
        console.log(admin)
    }
    return role
}

// FUNCIÓN PARA SABER SI ES DOCTOR
export function checkIsDoctor(){
    let role = null
    let admin = getIsAdmin()
    
    if(admin === 'doctor'){
        role = true
        console.log(admin)
    } else {
        role = false
        console.log(admin)
    }
    return role
}

// FUNCIÓN PARA SABER SI ES DOCTOR O PACIENTE
export function checkIsAutenticated(){
    let role = null
    let admin = getIsAdmin()
    
    if(admin.length > 1){
        role = true
    } else {
        role = false
    }
    return role
}


// FUNCIÓN DE GUARDAR EL NOMBRE DE USER EN LOCALSTORAGE
export function setName(name) {
    localStorage.setItem('NAME', name)
}
//FUNCIÓN PARA RECUPERAR EL NOMBRE DE USER EN LOCALSTORAGE
export function getName(){
    return localStorage.getItem('NAME')
}

//FUNCIÓN PARA RECUPERAR EL ID DE USER EN LOCALSTORAGE
export function getID(){
    return localStorage.getItem('ID')
}

// FUNCIÓN DE GUARDAR EL ID DE USER EN LOCALSTORAGE
export function setID(id) {
    localStorage.setItem('ID', id)
}
// FUNCIÓN DE GUARDAR EL PACK DE USER EN LOCALSTORAGE
export function setPACK(pack) {
    localStorage.setItem('PACK', pack)
}

// función de logout
export function logout(){
    axios.defaults.headers.common['Authorization'] = ''
    localStorage.removeItem('AUTH_TOKEN_KEY') // ELIMINAMOS EL TOKEN DE LOCALSTORAGE
    localStorage.removeItem('ROLE') // ELIMINAMOS EL ROL DE LOCALSTORAGE
    localStorage.removeItem('NAME') // ELIMINAMOS EL NOMBRE DE LOCALSTORAGE
    localStorage.removeItem('ID') // ELIMINAMOS EL ID DE LOCALSTORAGE
    localStorage.removeItem('PACK') // ELIMINAMOS EL PACK DE LOCALSTORAGE


}

