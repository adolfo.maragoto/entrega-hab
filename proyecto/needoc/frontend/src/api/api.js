
// Declarando cosas que instalé

const cors = require('cors')
const bodyParser = require('body-parser')
const mysql = require('mysql')
const express = require('express')
const app = express()

// Cosas que usa APP

app.use(cors())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

// conexión a la BBDD
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'notas'
})

// realizando conexión a BBDD
connection.connect(error => {
    if(error) throw error
    console.log('DATABASE CONECTADA :)')
})

// PUERTO DE CONEXIÓN DEL SERVICIO
const PORT = 3050

// CONEXIÓN DEL SERVICIO
app.listen(PORT, () => console.log('API CONECTADA :)'))



// RECOGER TODOS LOS CLIETES DE LA BASE
app.get('/', (req, res) => {
    res.send('Hola')
})

// RECOGIENDO TODOS LOS CLIENTES DE LA BASE DE DATOS
app.get('/clientes', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM lista_cliente'

    // conexion a BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay clientes que mostrar. :(')
        }
    })
})

// AÑADIR CLIENTES A LA BBDD
app.post('/add',(req, res) => {

    // SECUENCIA SQL
    const sql = "INSERT INTO lista_cliente SET ?"

    // OBJETO DE DATOS DEL NUEVO CLIENTE
    const nuevoCliente = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        ciudad: req.body.ciudad,
        empresa: req.body.empresa
    }

    // CONEXIÓN A BBDD
    connection.query( sql, nuevoCliente, error => {
        if(error) throw error
        console.log('Cliente credo con éxito :D')
    })

})

