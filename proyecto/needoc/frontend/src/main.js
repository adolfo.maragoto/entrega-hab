import Vue from 'vue'
import App from './App.vue'
import router from './router'

//vuetify - material design
// import vuetify from '@/plugins/vuetify' // path to vuetify export

// validacion formularios
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

// trabajar con fechas
import VueMoment from 'vue-moment'
const moment = require('moment')
require('moment/locale/es')

// traemos fontawesome
/*  // require('./plugins/fontawesome'); 
// traemos el plugin de fontawesome*/
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faUserSecret)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false


Vue.use(VueMoment, {
    moment,
})

import vueHeadful from 'vue-headful';// SEO poner titulos en las paginas
Vue.component('vue-headful', vueHeadful); 


// axios.interceptors.request.use(config => {
//   const token= localStorage.getItem('my-token-key') // or where you have the token
 
//   config.headers.common['Authorization'] = 'Bearer ' + token
//  console.log('main.js -->',config)
//   // you must return the config or it will not work
//   return config
//  })

Vue.config.productionTip = false
// vuetify, esto dentro de new vue
new Vue({
  // code fontasesome
  el: '#app',
  components: { App },
  template: '<App/>',
 // fin code fontasesome

  router,
  render: h => h(App)
}).$mount('#app')

