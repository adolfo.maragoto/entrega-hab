import Vue from 'vue'
import VueRouter from 'vue-router'
import { isLoggedIn } from '../../../backend/utils/utils'   // importamos la funcion isLoginIn
import { checkIsPatient } from '../../../backend/utils/utils'
import { checkIsDoctor } from '../../../backend/utils/utils'
import { checkIsAutenticated } from '../../../backend/utils/utils'


Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component:() => import('../views/Home.vue'),
    meta: { 
      allowAnon: true, // si es true cualquiera la puede ver
    }
  },
  {
    path: '/register',
    name: 'Register', // registro
    component:() => import('../views/Registro.vue'),
    meta: { 
      allowAnon: true, // si es true cualquiera la puede ver
    }
  },
  {
    path: '/login',
    name: 'Login', // login
    component:() => import('../views/Login.vue'),
    meta: { 
      allowAnon: true, // si es true cualquiera la puede ver
      onlyPatient: false, // solo los pacientes pueden acceder
      onlyDoctor: false // solo los pacientes pueden acceder
    }
  },
  {
    path: '/userprofile',
    name: 'User', // modificar usuario
    component:() => import('../views/ProfileUser.vue'),
    meta: {
      allowAnon: false, // si es true cualquiera la puede ver
      onlyPatient: true, // solo los pacientes pueden acceder
      onlyDoctor: false // solo los pacientes pueden acceder

    }
  },
  {
    path: '/docprofile',
    name: 'Doc', // modificar usuario
    component:() => import('../views/ProfileDoc.vue'),
    meta: {
      allowAnon: false, // si es true cualquiera la puede ver
      onlyPatient: false, // true los pacientes pueden acceder
      onlyDoctor: true // true los pacientes pueden acceder

    }
  },
  {
    path: '/doctor',
    name: 'doctor', // buscador doctores
    component:() => import('../views/Doctors.vue'),
    meta: {
      allowAnon: false, // si es true cualquiera la puede ver
      onlyAutenticated: true, // true los autenticados pueden acceder
    }
  },
  {
    path: '/doctor/file/',
    name: 'doctorfile', // buscador doctores
    component:() => import('../views/DoctorFile.vue'),
    meta: {
      allowAnon: false, // si es true cualquiera la puede ver
      onlyAutenticated: true, // true los autenticados pueden acceder
    }
  },
  {
    path: '/sentmail',
    name: 'sentmail', // ver lista mail enviados
    component:() => import('../views/MessagesSentEmail.vue'),
    meta: { 
      allowAnon: false, // si es true cualquiera la puede ver
      onlyAutenticated: true, // true los autenticados pueden acceder
    }
  },
  {
    path: '/send',
    name: 'Send', // enviar consulta
    component:() => import('../views/SendInquiry.vue'),
    meta: { 
      allowAnon: false, // si es true cualquiera la puede ver
      onlyAutenticated: true, // true los autenticados pueden acceder
    }
  },
  {
    path: '/inbox',
    name: 'Inbox', // modificar usuario
    component:() => import('../views/Inbox.vue'),
    meta: { 
      allowAnon: false, // si es true cualquiera la puede ver
      onlyAutenticated: true, // true los autenticados pueden acceder
    }
  },
  {
    path: '/inbox/:id_mess',
    name: 'InboxID', // ver consulta, enviar contestacion y enviar valoración
    component:() => import('../views/InboxID.vue'),
    meta: { 
      allowAnon: false, // si es true cualquiera la puede ver
      onlyAutenticated: true, // true los autenticados pueden acceder
    }
  },
  
  {
    path: '*',
    name: 'Error',
    component:() => import('../views/Error.vue')
  }
]


const router = new VueRouter({
  routes
})

// comprueba cosas antes de permitirte entrar en la ruta
router.beforeEach( (to, from, next) => {
  // traemos los datos de la funcion importada arriba  isLoggedIn
  // para saber si el usuario está logeado o no
  // con ello decidimos si puede pasar o no

  if(!to.meta.allowAnon && !isLoggedIn()) {
    next ({ // gestionamos si te dejo pasar o no
      path: '/', // aquí te lleva cuando te logueas
      query: { 
        redirect: to.fullPath 
      }
    })
  } else {
    next()
  }


  if(to.meta.onlyAutenticated === true && !checkIsAutenticated()){
    next({
      path:'/Error',
      query: { 
        redirect: to.fullPath 
      }
    })
  }
   else {
    next()
  }
  
  if(to.meta.onlyDoctor === true && !checkIsDoctor()){
    next({
      path:'/Error',
      query: { 
        redirect: to.fullPath 
      }
    })
  }
   else {
    next()
  }

  if(to.meta.onlyPatient === true && !checkIsPatient()){
        next({
          path:'/Error',
          query: { 
            redirect: to.fullPath 
          }
        })
      }
       else {
        next()
      }
} )

export default router
