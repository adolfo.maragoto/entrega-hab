-- usuario (#id, nombre, apellido, direccion, contrasena, email)
-- amigos (#id, -id_usuario, nombre, apellido, dni)
-- engargado (#id, nombre, apellido, direccion, contrasena, email)
-- restaurante (#id, nombre, telefono, capacidad, direccion, email, -tiempo_estancia, -hora, -dia)
-- reservas (#id, -id_user, dni_friends, name_friends, surname_friends)


USE entregableSQL;


SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE usuario (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
	apellido VARCHAR(50),
    direccion VARCHAR(40),
    contrasena VARCHAR(40) UNIQUE NOT NULL,
    email VARCHAR(50) NOT NULL
);

CREATE TABLE encargado (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
	apellido VARCHAR(50),
    direccion VARCHAR(40),
    contrasena VARCHAR(40) NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE amigos (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT UNSIGNED,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    nombre VARCHAR(40),
	apellido VARCHAR(40),
	dni VARCHAR(8) UNIQUE
);

CREATE TABLE reserva (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_user INT UNSIGNED,
    FOREIGN KEY (id_user) REFERENCES usuario(id),
    id_amigos INT UNSIGNED,
    FOREIGN KEY (id_amigos) REFERENCES amigos(id),
	día DATE NOT NULL,
    tiempo_estancia INT UNSIGNED NOT NULL,
	hora TIME NOT NULL
);

CREATE TABLE restaurante (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(40),
    telefono VARCHAR(40),
    capacidad INT UNSIGNED DEFAULT 0,
    dirección VARCHAR(40),
    id_reserva INT UNSIGNED,
    FOREIGN KEY (id_reserva) REFERENCES reserva(id)
);


SET FOREIGN_KEY_CHECKS = 1;
